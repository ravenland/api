var config = require('../config.json');
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : config.db.hostname,
  user     : config.db.username,
  password : config.db.password,
  database : config.db.name
});

function get_asset_id(req, res){
  // Function  will perform a DB query using the asset id provided
  connection.query('SELECT id, name, amount, units, has_ipfs, ipfs_hash, reissuable FROM assets WHERE id=' + req.params.id, function (error, results, fields) {
  if (error) throw error;
  response = results[0];
  res.send(response);
});
}

function get_all_assets(req, res){
  if (typeof req.params.paged === 'undefined' || req.params.paged === null) {
    req.params.paged = 0;
}

  connection.query('SELECT id, name, amount, units, has_ipfs, ipfs_hash, reissuable FROM assets limit ' + req.params.paged + ',1000', function (error, results, fields) {
  if (error) throw error;
  response = {}
  response['assets'] = results;
  response['paging'] = (Number(req.params.paged) + Number(1000));
  res.send(response);
});
}

function get_assetsbyaddress(req, res){
  // Function  will perform a DB query using the asset id provided
  connection.query('SELECT id, name, amount, units, has_ipfs, ipfs_hash, reissuable FROM assetaddresses WHERE id=' + req.params.id, function (error, results, fields) {
  if (error) throw error;
  response = results[0];
  res.send(response);
});
}



module.exports = function(server) {
  // server.get('/appstats', tester); NOT REQUIRED
  server.get('/asset/:id', get_asset_id)
  server.get('/assets', get_all_assets)
  server.get('/assets/:paged', get_all_assets)
  server.get('/assetsbyaddress/:id', get_assetsbyaddress)

};
