/*jslint node: true, indent: 2 */
/**
 * @author Michael Leer
 * @requires './config.json'
*/
'use strict';
var restify, bunyan, routing, log, server, config;

restify = require('restify');
routing = require('./routing/');
bunyan = require('bunyan');
config = require('./config.json');

/**
 * use Bunyan to log information
*/
log = bunyan.createLogger({
  name: config.name,
  streams: [
  {
    path: config.log_location + 'info.json',
    level: 'info'
  }, {
    path: config.log_location + 'error.json',
    level: 'error'
  }, {
    path: config.log_location + 'fatal.json',
    level: 'fatal'
  }, {
    path: config.log_location + 'test.json',
    level: 'warn'
  }],
  serializers: bunyan.stdSerializers
});

server = restify.createServer({
  name: config.name,
  log: log,
  formatters: {
    'application/json': function(req, res, body) {
      //res.setHeader('Cache-Control', 'must-revalidate');
      // ONLY send JSON
      res.setHeader('Content-Type', 'application/json');
      return JSON.stringify(body);
    }
  }
});

server.pre(restify.pre.sanitizePath());

/*jslint unparam:true*/

var auditor = bunyan.createLogger({
  name: config.name + '- Auditor',
  stream: process.stdout,
  serializers: bunyan.stdSerializers
});

routing(server);

server.listen(config.port, function() {
  log.debug('%s listening at %s', server.name, server.url);
});

console.log('Server started, on port %s', server.url);
