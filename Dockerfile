FROM node:8
ADD . /srv
WORKDIR /srv
RUN yarn install
CMD ["node", "server.js"]
